import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView
} from 'react-native';

import Modal from 'react-native-modal';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

export default class BusinessInfo extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      title: 'Sno-Boss',
      subtitle: 'Boss Flavors',
      description: 'Banana, Bahama Mama, Blackberry, Black, Cherry, Blue Raspberry, Bubble Gum, Cantaloupe, Cherry, Cheesecake, Chocolate, Coconut, Cotton Candy, Dreamsiclem Fuzzy Navel, Banana, Bahama Mama, Blackberry, Black, Cherry, Blue Raspberry, Bubble Gum, Cantaloupe, Cherry, Cheesecake, Chocolate, Coconut, Cotton Candy, Dreamsiclem Fuzzy Navel, Banana, Bahama Mama, Blackberry, Black, Cherry, Blue Raspberry, Bubble Gum, Cantaloupe, Cherry, Cheesecake, Chocolate, Coconut, Cotton Candy, Dreamsiclem Fuzzy Navel, Banana, Bahama Mama, Blackberry, Black, Cherry, Blue Raspberry, Bubble Gum, Cantaloupe, Cherry, Cheesecake, Chocolate, Coconut, Cotton Candy, Dreamsiclem Fuzzy Navel, Banana, Bahama Mama, Blackberry, Black, Cherry, Blue Raspberry, Bubble Gum, Cantaloupe, Cherry, Cheesecake, Chocolate, Coconut, Cotton Candy, Dreamsiclem Fuzzy Navel, Banana, Bahama Mama, Blackberry, Black, Cherry, Blue Raspberry, Bubble Gum, Cantaloupe, Cherry, Cheesecake, Chocolate, Coconut, Cotton Candy, Dreamsiclem Fuzzy Navel, Banana, Bahama Mama, Blackberry, Black, Cherry, Blue Raspberry, Bubble Gum, Cantaloupe, Cherry, Cheesecake, Chocolate, Coconut, Cotton Candy, Dreamsiclem Fuzzy Navel,'
    }
  }
  render() {
    return(
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.title}>
            {this.state.title}
          </Text>
          <Text style={styles.subtitle}>
            {this.state.subtitle}
          </Text>
          <Text style={styles.description}>
            {this.state.description}
          </Text>
        </View>
        
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 20
  },
  title: {
    marginTop: 20,
    fontSize: 18,
    fontWeight: 'bold'
  },
  subtitle: {
    marginTop: 10
  },
  description: {
    marginTop: 20,
    textAlign: 'center'
  }
})

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Button,
  Image
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import call from 'react-native-phone-call';
import email from 'react-native-email';

export default class ContactUs extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      phone: '+1234567890',
      email: 'events@sno-Boss.com',
      visit: 'https://google.com'
    }
  }

  handleContact = (icon) => {
    if(icon === 'phone') {
      const args = {
        number: this.state.phone,
        prompt: true
      }
      call(args).catch(console.error)
    }
    if(icon === 'email') {
      const to = [this.state.email] // string or array of email addresses
        email(to, {
            subject: 'Show how to use',
            body: 'Some body right here'
        }).catch(console.error)
    }
  }

  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.title}>Contact Us</Text>
        {this.renderContact('phone', 'Call Us on:', this.state.phone)}
        {this.renderContact('email', 'Email Us on:', this.state.email)}
        {this.renderContact('earth', 'Visit:', this.state.visit)}
        
        <Text style={styles.followText}>Follow Us On:</Text>
        <View style={styles.follows}>
          {this.renderFollows('facebook-box')}
          {this.renderFollows('twitter-box')}
          {this.renderFollows('instagram')}
        </View>
        
      </View>
    )
  }

  renderContact = (icon, title, detail) => {
    return(
      <View style={styles.contactView}>
        <TouchableOpacity style={styles.iconView} onPress={() => this.handleContact(icon)}>
          <MaterialCommunityIcons name={icon} size={20} color={'white'} />
        </TouchableOpacity>
        <View style={styles.detail}>
          <Text style={styles.detailTitle}>{title}</Text>
          <Text style={styles.detailValue}>{detail}</Text>
        </View>
      </View>
    )
  }

  renderFollows = (icon) => {
    return(
      <TouchableOpacity style={styles.follow_btn} >
        <MaterialCommunityIcons name={icon} size={40} color={'#54e3b8'} />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15
  },
  title: {
    paddingVertical: 20,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  contactView: {
    width: '100%',
    height: 60,
    flexDirection: 'row',
    alignItems:'center',
    backgroundColor: 'white',
    borderRadius: 5,
    marginBottom: 10
  },
  iconView: {
    marginHorizontal: 10,
    width: 40,
    height: 40,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#54e3b8',
  },
  detailTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#73776b'
  },
  detailValue: {
    marginTop: 8,
    fontSize: 14,
    fontWeight: 'bold',
  },
  followText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  follow_btn: {
    width: '33%',
    backgroundColor: 'white',
    height: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  follows: {
    marginTop: 6,
    flexDirection: 'row', 
    justifyContent: 'space-between'
  }
})

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions
} from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default class Schedule extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      upcomingEvents: [
        {title: 'Title1', date: '8/4/2019', details: 'Details 1'},
        {title: 'Title2', date: '9/4/2019', details: 'Details 2'},
        {title: 'Title3', date: '10/4/2019', details: 'Details 3'},
        {title: 'Title4', date: '11/4/2019', details: 'Details 4'},
        {title: 'Title5', date: '12/4/2019', details: 'Details 5'},
        {title: 'Title6', date: '1/4/2020', details: 'Details 6'}
      ]
    }
  }

  handleOpenMadal = () => {
    this.setState({modalVisible: !this.state.modalVisible})
  }

  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.scheduletitle}>Upcoming Events</Text>
        <ScrollView style={styles.events}>
          <View style={styles.line} />
          {this.state.upcomingEvents.map((event, index) => {
            return(
              index % 2 === 0 ?
              <View key={index} style={styles.rightEvent}>
                <View style={styles.right_detail}>
                  <Text style={styles.right_detail_text}>{event.title}</Text>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <Text style={styles.right_detail_text}>{event.date}</Text>
                    <FontAwesome style={{marginLeft: 5}} name='calendar' size={14} color='#73776b' />
                  </View>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <Text style={[styles.right_detail_text, {marginTop: 15}]}>{event.details}</Text>
                    <FontAwesome5 name='clock' size={14} color='#73776b' />
                  </View>
                </View>
                <View style={styles.marker}>
                  <FontAwesome5 name='map-marker-alt' size={14} color='white' />
                </View>
              </View>
              :
              <View key={index} style={styles.leftEvent}>
                <View style={styles.marker}>
                  <FontAwesome5 name='map-marker-alt' size={14} color='white' />
                </View>
                <View style={styles.left_detail}>
                  <Text style={styles.left_detail_text}>{event.title}</Text>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <FontAwesome name='calendar' size={14} color='#73776b' />
                    <Text style={[styles.left_detail_text, {marginLeft: 5}]}>{event.date}</Text>
                  </View>
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <FontAwesome5 name='clock' size={14} color='#73776b' />
                    <Text style={[styles.left_detail_text, {marginTop: 15}]}>{event.details}</Text>
                  </View>
                </View>
              </View>
            )
          })}
          <View style={styles.seeall}>
            <Text style={[styles.scheduletitle, {fontSize: 16, paddingVertical: 0}]}>See all Events</Text>
            <TouchableOpacity style={styles.seeall_btn}>
              <FontAwesome name='angle-double-down' size={16} color='#54e3b8'/>
            </TouchableOpacity>
            
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scheduletitle: {
    paddingVertical: 20,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  events: {
    width: '100%',
    paddingBottom: 40
  },
  line: {
    position: 'absolute',
    width: 0.5,
    height: '100%',
    alignSelf: 'center',
    backgroundColor: '#73776b'
  },
  rightEvent: {
    marginTop: 10,
    marginBottom: 20,
    width: Dimensions.get('window').width/2 - 3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  right_detail: {
    paddingHorizontal: 10,
    alignItems: 'flex-end'
  },
  right_detail_text: {
    color: '#73776b',
    fontSize: 14,
    textAlign: 'right',
    paddingLeft: 50
  },
  marker: {
    width: 30,
    height: 30,
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#54e3b8'
  },
  leftEvent: {
    marginTop: 10,
    marginBottom: 20,
    width: Dimensions.get('window').width/2 - 3,
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  left_detail: {
    marginLeft: 10,
  },
  left_detail_text: {
    color: '#73776b',
    fontSize: 14,
    paddingRight: 50
  },
  seeall: {
    alignSelf: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  seeall_btn: {
    width: 30,
    height:30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: '#73776b',
    borderWidth:0.5
  }
})

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  PanResponder,
  Image
} from 'react-native';

import Modal from 'react-native-modal';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
import LinearGradient from 'react-native-linear-gradient';

import BusinessInfo from './screens/businessinfo';
import Schedule from './screens/schedule';
import ContactUs from './screens/contactus';

export default class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      rank: 0,
      favorites: 3,
      isFavorite: false,
      footer: [
        'Business Info',
        'Schedule',
        'Contact Us',
      ],
      footerIndex: 0,
      firstX: 0
    }
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (event, gestureState) => true,
      onPanResponderStart: (event, gestureState) => {
        this.setState({firstX: gestureState.x0});
      },
      onPanResponderEnd: (event, gestureState) => {
        if(gestureState.moveX > this.state.firstX) {
          this.state.footerIndex !== 2 && this.setState({footerIndex: this.state.footerIndex + 1})
        }
        if(gestureState.moveX < this.state.firstX) {
          this.state.footerIndex !== 0 && this.setState({footerIndex: this.state.footerIndex - 1})
        }
      },
    })
  }

  handleOpenMadal = () => {
    this.setState({modalVisible: !this.state.modalVisible});
  }

  handleFavorite = () => {
    this.setState({isFavorite: !this.state.isFavorite});
  }

  handlePrev = () => {
    this.setState({footerIndex: this.state.footerIndex - 1});
  }

  handleNext = () => {
    this.setState({footerIndex: this.state.footerIndex + 1});
  }

  render() {
    return(
      <View style={styles.container}>
        <TouchableOpacity  style={styles.btn_showmodal} onPress={() => this.handleOpenMadal()}>
          <Text>Modal Show</Text>
        </TouchableOpacity>
        <Modal
          animationInTiming={1000}
          animationType={"slide"}
          transparent={true}
          isVisible={this.state.modalVisible}
          onRequestClose={() => this.setState({modalVisible: false})}
          onBackdropPress={() => this.setState({modalVisible: false})}
          onSwipeComplete={() => this.setState({modalVisible: false})}
        >
          <View style={styles.modalContainer}>
            <Image style={styles.truckImage} source={require('./images/truck.png')} />
            <View style={styles.modalheader}>
              <View style={styles.modalheader_view1}>
                <View style={styles.rf_view}>
                  <Text style={styles.rf_text}>Rank</Text>
                  <View style={styles.rf_num}>
                    <Text style={styles.rf_num_text}>{this.state.rank}</Text>
                  </View>
                </View>
                <View style={styles.rf_view}>
                  <Text style={styles.rf_text}>Favorites</Text>
                  <View style={styles.rf_num}>
                    <Text style={styles.rf_num_text}>{this.state.favorites}</Text>
                  </View>
                </View>
                <TouchableOpacity style={styles.star} onPress={() => this.handleFavorite()}>
                  <FontAwesome name={this.state.isFavorite ? 'star' : 'star-o'} size={14} color={this.state.isFavorite? '#f2cc48' : '#333'} />
                </TouchableOpacity>
              </View>
              <View style={styles.modalheader_view2}>
                <TouchableOpacity>
                  <LinearGradient
                    colors={['#25a573', '#46cf9a']}
                    style={styles.modalheader_btn}
                    start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                  >
                    <Text style={styles.modalheader_btn_text}>Order Food</Text>
                  </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity>
                  <LinearGradient
                    colors={['#af7296', '#d1a3be']}
                    style={styles.modalheader_btn}
                    start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                  >
                    <Text style={styles.modalheader_btn_text}>Direction</Text>
                  </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity>
                  <LinearGradient
                    colors={['#61a1ce', '#86cdff']}
                    style={styles.modalheader_btn}
                    start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                  >
                    <Text style={styles.modalheader_btn_text}>Schedule</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.modalBody}>
              {this.state.footerIndex === 0 && <BusinessInfo />}
              {this.state.footerIndex === 1 && <Schedule />}
              {this.state.footerIndex === 2 && <ContactUs />}
            </View>
            <View style={styles.modalFooter} {...this._panResponder.panHandlers}>
              <View style={{width: 100, justifyContent: 'flex-start'}}>
                {this.state.footerIndex !== 0 && <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => this.handlePrev()}>
                  <FontAwesome name='angle-double-left' size={16} color='#000'/>
                  <Text style={styles.footerText}>{this.state.footer[this.state.footerIndex - 1]}</Text>
                </TouchableOpacity>}
              </View>
              <View style={{flexDirection: 'row'}}>
                <Octicons name='primitive-dot' size={16} color={this.state.footerIndex === 0 ? '#fff': 'rgba(255, 255, 255, 0.4)'}/>
                <Octicons style={{marginHorizontal: 7}} name='primitive-dot' size={16} color={this.state.footerIndex === 1 ? '#fff': 'rgba(255, 255, 255, 0.4)'}/>
                <Octicons name='primitive-dot' size={16} color={this.state.footerIndex === 2 ? '#fff': 'rgba(255, 255, 255, 0.4)'}/>
              </View>
              <View style={{width: 100, alignItems: 'flex-end'}}>
                {this.state.footerIndex !== 2 && <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => this.handleNext()}>
                  <Text style={styles.footerText}>{this.state.footer[this.state.footerIndex + 1]}</Text>
                  <FontAwesome name='angle-double-right' size={16} color='#000'/>
                </TouchableOpacity>}
              </View>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn_showmodal: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4cdfa9'
  },
  modalContainer: {
    width: '100%',
    height: '90%',
    marginHorizontal: 10,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: 'white'
  },
  truckImage: {
    width: 60,
    height: 60,
    position: 'absolute',
    top: -30,
    resizeMode: 'contain'
  },
  modalheader: {
    width: '100%',
    height: '14%',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
    paddingVertical: 10
  },
  modalheader_view1: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  rf_view: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rf_text: {
    
  },
  rf_num: {
    marginLeft: 5,
    width: 16,
    height: 16,
    borderRadius: 8,
    backgroundColor: '#4cdfa9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  rf_num_text: {
    fontSize: 12,
    color: 'white'
  },
  star: {
    position: 'absolute',
    right: -17
  },
  modalheader_view2: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  modalheader_btn: {
    width: 90,
    justifyContent: 'center',
    alignItems: 'center',
    height: 20,
    borderRadius: 10,
  },
  modalheader_btn_text: {
    fontSize: 10,
    color: 'white'
  },
  modalBody: {
    backgroundColor: '#f6f6f6',
    width: '100%',
    height: '82%'
  },
  modalFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: '5%',
    paddingHorizontal: 15,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: '#54e3b8'
  },
  footerText: {
    marginHorizontal: 5
  }
})
